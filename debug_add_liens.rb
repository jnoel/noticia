#!/usr/bin/env ruby
# encoding: UTF-8

require "./noticia.rb"

if ARGV.count.eql?(3)
	titre = ARGV[0]
	lien = ARGV[1]
	groupe_id = ARGV[2].to_i
	nouveau_lien_sans_protection titre, lien, groupe_id, verbose=true
else
	puts "Nombre d'arguments incorrect. Il doit y en avoir 3 : titre, lien, groupe_id"
end
