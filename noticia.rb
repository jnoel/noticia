#!/usr/bin/env ruby
# encoding: UTF-8

require "bundler/setup"
require "sinatra"
#require "sinatra/reloader" if development?
require "sinatra/activerecord"
require "simple-rss"
require "open-uri"
require "haml"
require "htmlentities"
require "./db/models.rb"
require "./helper.rb"

set :username,'admin'
set :password,'admin'
set :token,'sfghdfh@ðđŋdkgkl54%qdse'

get '/' do
	protected!
	sites = Flux.order(:ordre)
	@flux = []
	sites.each do |site|
		articles = Article.where(:flux_id => site.id).order(:id).limit(10)
		@flux << {:titre => site.titre,
							:lien => site.lien,
							:id => site.id,
							:articles => articles
						 }
	end
	haml :index
end

get '/add_flux' do
	protected!
	haml :add_flux
end

post '/add_flux' do
	protected!
	flux = params[:rss]
	nouveau_flux flux
	redirect '/'
end

get '/modifier_flux/:num' do
	protected!
	@flux = Flux.where(:id => params[:num]).first
	if @flux
		haml :modifier_flux
	else
		redirect '/'
	end
end

get '/add_lien' do
	protected!
	haml :add_lien
end

post '/add_lien' do
	protected!
	titre = params[:titre]
	lien = params[:lien]
	groupe_id = params[:groupe].to_i
	nouveau_lien titre, lien, groupe_id unless titre.empty? or lien.empty?
	redirect '/'
end

post '/recherche' do
	protected!
	termes = params[:search].gsub(" ", "+")
	redirect "https://www.google.fr/#hl=fr&q=#{termes}"
	#redirect "https://www.qwant.com/?q=#{termes}"
end

get '/refresh' do
	protected!
	haml :refresh
end

post '/refresh' do
	protected!
	maj
	redirect '/'
end

get '/about' do
	haml :about
end

post '/retour' do
	protected!
	redirect '/'
end

get '/add_groupe' do
	protected!
	haml :add_groupe
end

post '/add_groupe' do
	protected!
	titre = params[:titre]
	nouveau_groupe titre unless titre.empty?
	redirect '/add_lien'
end

get '/login' do
	haml :login 
end

post '/login' do
  token = get_token params['username'], params['password']
  unless token.nil?
    response.set_cookie(params['username'],{:value => token, :expires => (Time.now + 52*7*24*60*60)}) 
    redirect '/'
  else
      "Code utilisateur ou mot de passe incorrect"
  end
end

get '/logout' do 
	protected!
	haml :logout
end

post '/logout' do 
	response.set_cookie(settings.username, false)
	redirect '/'
end
