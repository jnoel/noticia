# encoding: UTF-8

def admin?
	request.cookies[settings.username] == settings.token 
end

def protected! 
	redirect "/login" unless admin?
end

def get_token username, password
	if username == settings.username && password == settings.password
		return settings.token
	else
		return nil
	end
end

def nouveau_flux site	
	begin
		nouveau_flux_sans_protection site
	rescue
		message "Le flux n'a pas pu être correctement ajouté"
	end
end

def nouveau_flux_sans_protection site, verbose=false
	flux = SimpleRSS.parse(open(site))
	f = Flux.new
	begin
		coder = HTMLEntities.new
		f.titre = coder.decode flux.feed.title
	rescue
		f.titre = flux.feed.title
	end
	message "titre = #{f.titre}" if verbose
	f.lien = flux.feed.link
	message "lien = #{f.lien}" if verbose
	f.rss = site
	message "rss = #{f.rss}" if verbose
	res = f.save
	if verbose
		message (res ? "Enregistrement effectué" : "Erreur à l'enregistrement")
		message "---"
	end
	refresh_articles flux, f.id, verbose
end

def remove_flux_sans_protection id, verbose=false
	if Article.where(:flux_id => id).destroy_all
		message "Articles associés au flux #{id} supprimés" if verbose
		if Flux.find(id).destroy
			message "Flux #{id} supprimé" if verbose
			message "---" if verbose
		else
			message "Erreur le flux #{id} n'a pas été supprimé"
		end
	else
		message "Erreur les articles associés au flux #{id} n'ont pas été supprimés"
	end
end

def nouveau_lien titre, lien, groupe_id	
	begin
		nouveau_lien_sans_protection titre, lien, groupe_id	
	rescue
		message "Le lien n'a pas pu être correctement ajouté"
	end
end

def nouveau_lien_sans_protection titre, url, groupe_id, verbose=false
	message titre if verbose
	message url if verbose
	message groupe_id if verbose
	lien = Lien.new
	lien.titre = titre
	lien.lien = url
	lien.groupe_id = groupe_id
	lien.save
end

def remove_lien_sans_protection id, verbose=false
	if Liens.find(id).destroy
		message "Lien #{id} supprimé" if verbose
		message "---" if verbose
	else
		message "Erreur le lien #{id} n'a pas été supprimé"
	end
end

def refresh_flux id, verbose=false
	site = Flux.find(id)
	flux = SimpleRSS.parse(open(site.rss))
	message "Mise à jour du flux #{id}" if verbose
	refresh_articles flux, id, verbose
end

def refresh_articles flux, id, verbose=false
	Article.where(:flux_id => id).destroy_all
	flux.entries.each do |entrie|
		a = Article.new
		begin
			coder = HTMLEntities.new
			a.titre = coder.decode entrie.title
		rescue
			a.titre = entrie.title
		end
		message "---titre = #{a.titre}" if verbose
		lien = entrie.link
		# Si l'url finale est intégrée dans une url -- par exemple google actu -- on la sort.
		unless lien.nil?
			if lien.include?(";url=")
				tab = lien.split(";")
				tab.each do |t|
					if t.include?("url=")
						lien = t.gsub("url=","")
					end
				end
			end
		end
		a.lien = lien
		message "---lien = #{a.lien}" if verbose
		a.flux_id = id
		message "---flux_id = #{a.flux_id}" if verbose
		res = a.save
		if verbose
			message (res ? "---Enregistrement effectué" : "Erreur à l'enregistrement")
		end
	end
	message "---\n" if verbose
end

def maj
	sites = Flux.order(:ordre)

	sites.each do |site|
		begin
			refresh_flux site.id, true
		rescue
			message "Erreur dans la récupération du flux"
		end
	end
end

def nouveau_groupe titre, verbose=false
	message titre if verbose
	groupe = Groupe.new
	groupe.titre = titre
	groupe.save
end

#---------- FORMULAIRES ---------------

def groupe_options
	haml = ""
	groupes = Groupe.order(:titre)
	groupes.each do |groupe|
		haml << "%option{:value => '#{groupe.id}'}\n	#{groupe.titre}\n"
	end
	return haml
end

#---------- BANDEAU DROIT -------------

def liens
	
	haml = ""
	groupes = Groupe.order(:ordre)
	
	groupes.each do |groupe|
		l = ""
		liens = Lien.where(:groupe_id => groupe.id).order(:titre)
		liens.each do |lien|
			if groupe.affiche_texte
				l << "%a{:href=>'#{lien.lien}', :target => '_blank'}\n	#{lien.titre} - \n"
			elsif groupe.affiche_image
				l << "%a{:href=>'#{lien.lien}', :target => '_blank'}\n	%img{:src => 'images/chaines/#{lien.image}', :title =>'#{lien.titre}'}\n"
			end
		end
		haml << "%h1		#{groupe.titre}\n#{l}\n" if liens.count>0
	end
	
	return haml
	
end

def message texte

	puts texte
	
end
