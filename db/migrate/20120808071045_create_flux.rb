class CreateFlux < ActiveRecord::Migration
  def up
  	create_table :flux do |t|
      t.string :titre
      t.string :lien
      t.string :rss
	end
  end

  def down
  	drop_table :flux
  end
end
