class CreateArticles < ActiveRecord::Migration
  def up
  	create_table :articles do |t|
      t.string :titre
      t.string :lien
      t.integer :flux_id
	end
  end

  def down
  	drop_table :articles
  end
end
