class CreateLiens < ActiveRecord::Migration
  def up
  	create_table :liens do |t|
      t.string :titre
      t.string :lien
      t.string :image
      t.boolean :chaine, :default => false
	end
  end

  def down
  	drop_table :liens
  end
end
