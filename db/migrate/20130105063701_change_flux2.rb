class ChangeFlux2 < ActiveRecord::Migration
  def up
  	change_table :flux do |t|
	  	t.change :ordre, :float
		end
  end

  def down
  	change_table :flux do |t|
			t.change :ordre, :integer
		end
  end
end
