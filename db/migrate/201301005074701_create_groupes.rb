class CreateGroupes < ActiveRecord::Migration
  
  def up
  	
  	create_table :groupes do |t|
      t.string :titre
      t.float :ordre, :default => 999
      t.boolean :affiche_texte, :default => true
      t.boolean :affiche_image, :default => false
		end
		
		Groupe.create :titre => "Divers", :ordre => 999
		
  end
  
  def down
  	drop_table :groupes
  end
  
end
