class ChangeLiens < ActiveRecord::Migration
  def up
  	change_table :liens do |t|
	  	t.integer :groupe_id, :default => 1
		end
  end

  def down
  	change_table :liens do |t|
			t.remove :groupe_id
		end
  end
end
