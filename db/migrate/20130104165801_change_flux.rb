class ChangeFlux < ActiveRecord::Migration
  def up
  	change_table :flux do |t|
	  	t.integer :ordre, :default => 999
		end
  end

  def down
  	change_table :flux do |t|
			t.remove :ordre
		end
  end
end
