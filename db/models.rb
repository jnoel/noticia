
# ---------- CONFIGURATION DE LA BASE DE DONNEES -------------------------------

ActiveRecord::Base.establish_connection(
	:adapter => 'mysql2',
	:host =>  '127.0.0.1',
	:database =>  'noticia',
	:username =>  'noticia',
	:password =>  'N,[}2 !10z&x56F'
)

# ---------- FIN DE LA CONFIGURATION DE LA BASE DE DONNEES ---------------------

class Flux < ActiveRecord::Base
	has_many :articles
	self.pluralize_table_names = false
end

class Article < ActiveRecord::Base
	belongs_to :flux
end

class Groupe < ActiveRecord::Base
	has_many :liens
end

class Lien < ActiveRecord::Base
	belongs_to :groupe
end
